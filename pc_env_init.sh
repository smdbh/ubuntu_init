#!/bin/bash

echo "ubuntu init env start..."

CODENAME=$(lsb_release -c | awk '{print $2}')
USERNAME=""
PASSWD=""
HOMEDIR=~  # /home/${username}


check_passwd()
{
    if [[ -z "$PASSWD"  ||  -z "$USERNAME" ]]; then
        echo "need set a user and password!"
        read -p "user: " USERNAME
        read -s -p "passwd:" PASSWD
        read -s -p "repeat passwd:" PASSWD2
        
        if [ "$PASSWD" != "$PASSWD2" ]; then
            echo "password not same, exit"
            exit
        fi
    fi
}

# 添加一个默认用户 ---------------------------------------------------------------
add_default_user()
{
    echo "create a default user:"
    read -p "user: " USERNAME

    check_passwd # will exit if not math

    useradd -m ${USERNAME}
    passwd ${USERNAME}
    usermod -aG sudo ${USERNAME}
    #sudo chsh -s $(which zsh) $USERNAME
    HOMEDIR=/home/${USERNAME}           # change home directory

}


#--换源 ---------------------------------------------------------------------------
APT_DOMAIN='https://mirrors.ustc.edu.cn/ubuntu/'
APT_LIST="
deb ${APT_DOMAIN} $CODENAME main restricted universe multiverse
deb ${APT_DOMAIN} $CODENAME-updates main restricted universe multiverse
deb ${APT_DOMAIN} $CODENAME-backports main restricted universe multiverse
deb ${APT_DOMAIN} $CODENAME-security main restricted universe multiverse
"
update_aptsourcelist()
{
# sed -i 's#http://deb.debian.org#https://mirrors.ustc.edu.cn#' /etc/apt/sources.list
    #apt source.  force set
    if [ ! -f /etc/apt/sources.list.bak ]; then
        sudo cp /etc/apt/sources.list  /etc/apt/sources.list.bak
        echo 'backup sourcelist file'
    fi
    echo "$APT_LIST" | sudo tee   /etc/apt/sources.list
}


#--安装软件------------------------------------------------------------------------
APT_SOFTWARE=" byobu vim samba zsh htop zip unzip curl fzf
		git wget tree net-tools openssh-server locate silversearcher-ag"

function install_apt()
{
    sudo apt-get update
    sudo apt-get install -y $APT_SOFTWARE

	# apply new changes
	service ssh restart
    # fix config 
    _file=/etc/proxychains4.conf
    if [ -f $_file ]; then
        sed -E  's/(^socks4.+)1080$/\1 1089/' $_file
    fi	
}

# set zsh -------------------------------------------------------------------
function install_oh_my_zsh()
{
    echo "zsh dir: " ${HOMEDIR}/oh-my-zsh

    if [ ! -d ${HOMEDIR}/oh-my-zsh ]; then

		git clone https://gitee.com/mirrors/oh-my-zsh.git  ${HOMEDIR}/oh-my-zsh
		cd ${HOMEDIR}/oh-my-zsh/custom/plugins/
		
		git clone https://gitee.com/simonliu009/zsh-syntax-highlighting  
		git clone https://gitee.com/mirror-hub/zsh-history-substring-search
		cd ${HOMEDIR}
    fi
    
    # zsh, default
    check_passwd
    sudo chsh -s $(which zsh) $USERNAME

}

# set samba -------------------------------------------------------------------
MYSMB_CONF="
[home_workspaces]
    comment = %U workspace
    browseable = yes
    guest ok = no
    writable = yes
    valid users = %S
    create mask = 0755
    directory mask = 0755
"

function config_samba()
{   
    echo "samba start..."
    # not found will return 1 ,and cause script end
    # NOT CHANGE! homes is samba key word, 
    ret=$(grep homes -c /etc/samba/smb.conf)||true
    
    if [ $ret -eq 0 ]; then
        echo 'update samba conf.'
        check_passwd
        echo  "$MYSMB_CONF" |sudo tee /etc/samba/smb.conf
    fi

    echo "debug>" $PASSWD
    (echo $PASSWD;echo $PASSWD) | sudo smbpasswd -s  -a $USERNAME

    sudo service smbd restart
}





## run init function steps  ##################################################

update_aptsourcelist
# install_apt
# add_default_user
# install_oh_my_zsh
# config_samba

